extends Control

var zoom = 1.0
var latest_path = "C:/"
var og_size = Vector2(ProjectSettings.get_setting("display/window/size/width"), ProjectSettings.get_setting("display/window/size/height"))
var zoomed_in = false
var movable = false
var moved = false
var moving = false
var current_cursor = 0
var current_dir = ""
var image_path = ""
var images = []
var current_image = 0
var top_menu_color = "d8e4f2"
var og_top_menu_color = "d8e4f2"
var text_color = "3D3D3D"
var og_text_color = "3d3d3d"
var background_color = "eff2f9"
var og_background_color = "eff2f9"
var remember_zoom = false
const text_theme = "res://text_color.tres"
const dir_save_path = "user://latest_dir.data"
const preferences_save_path = "user://preferences.data"

func _ready():
	OS.set_window_title("Tezlz Image Viewer")
	if OS.get_name() != "Windows":
		latest_path = "/home/"
	image_path = "res://icon.png"
	current_dir = get_dir(image_path)
	images = list_files_in_directory(current_dir)
	current_image = item_index(images, img_name(image_path))
	load_dir()
	load_preferences()
	# Signals för alla Menu items
	$"Menu/MenuButtonContainer/MenuButtonFile".get_popup().connect("id_pressed", self, "_on_menu_file_pressed")
	$"Menu/MenuButtonContainer/MenuButtonEdit".get_popup().connect("id_pressed", self, "_on_menu_edit_pressed")
	$"Menu/MenuButtonContainer/MenuButtonView".get_popup().connect("id_pressed", self, "_on_menu_view_pressed")
	$"Menu/MenuButtonContainer/MenuButtonHelp".get_popup().connect("id_pressed", self, "_on_menu_help_pressed")
	get_tree().get_root().connect("size_changed", self, "_on_window_resized")
	$"Menu/MenuBar".connect("mouse_entered", self, "_on_Menu_mouse_entered")
	$"Menu/MenuBar".connect("mouse_exited", self, "_on_Menu_mouse_exited")
	var menu_buttons = $"Menu/MenuButtonContainer".get_children()
	for button in range(0, len(menu_buttons)):
		$"Menu/MenuButtonContainer".get_child(button).connect("mouse_entered", self, "_on_Menu_mouse_entered")
		$"Menu/MenuButtonContainer".get_child(button).connect("mouse_exited", self, "_on_Menu_mouse_exited")
	$"Menu/FileDialogOpen".add_filter("*.png ; PNG Images")
	$"Menu/FileDialogOpen".add_filter("*.jpg ; JPEG Images")
	$"Menu/MenuBar".set_size(Vector2(get_viewport().size.x, 35))
	$"LowerMenu/CollisionBar".set_size(Vector2(get_viewport().size.x, 20))
	$"LowerMenu/CollisionBar".set_position(Vector2(0, get_viewport().size.y-20))
	$"Menu/WindowPreferences/TopBarColor".set_pick_color(top_menu_color)
	$"Menu/WindowPreferences/BackgroundColor".set_pick_color(background_color)
	$"Menu/WindowPreferences/TopBarTextColor".set_pick_color(text_color)
	$"LowerMenu/PopupMenuContainer".set_position(Vector2((get_viewport().size.x/2)-100, get_viewport().size.y+42))
	$Image.set_position(Vector2(get_viewport().size.x/2, (get_viewport().size.y+35)/2))
	$ImageCenter.set_position(Vector2(get_viewport().size.x/2, (get_viewport().size.y+35)/2))


func _process(delta):
	if check_if_image_is_zoomed_in():
		if Input.is_action_pressed("move"):
			if not moving:
				$Image.change_offset()
				moving = true
			moved = true
			$Image.set_position(get_global_mouse_position())
		else:
			if moving:
				moving = false
		if current_cursor == 0:
#			print("Zoomed in!")
			Input.set_custom_mouse_cursor(load("res://cursor_move.png"))
			current_cursor = 1
	else:
		if current_cursor == 1:
#			print("Zoomed out!")
			Input.set_custom_mouse_cursor(load("res://cursor_arrow.png"))
			current_cursor = 0
	if Input.is_action_just_pressed("ui_right"):
		if not current_image == len(images)-1:
			current_image += 1
		else:
			current_image = 0
		_on_FileDialogOpen_file_selected(current_dir + images[current_image])
	elif Input.is_action_just_pressed("ui_left"):
		if not current_image == 0:
			current_image -= 1
		else:
			current_image = len(images)-1
		_on_FileDialogOpen_file_selected(current_dir + images[current_image])
	if Input.is_action_just_pressed("toggle_fullscreen"):
		OS.window_fullscreen = not OS.window_fullscreen
	$Image.scale = Vector2(zoom, zoom)


func _input(event):
	if not $"Menu/FileDialogOpen".visible:
		if event is InputEventMouseButton:
			if event.is_pressed():
				if event.button_index == BUTTON_WHEEL_UP:
					zoomed_in = true
					if check_if_image_is_zoomed_in():
						$Image.change_offset()
						$Image.set_position(get_global_mouse_position())
					zoom /= 0.8
				elif event.button_index == BUTTON_WHEEL_DOWN:
					if check_if_image_is_zoomed_in():
						$Image.change_offset()
						$Image.set_position(get_global_mouse_position())
					zoom *= 0.8


func check_if_image_is_zoomed_in():
	if ($Image.get_texture().get_height())*zoom > get_viewport().size.y-35:
		return true
	else:
		return false

func get_image_texture():
	return $Image.get_texture()

func get_corner_coords():
	return $Image.get_position()


func load_dir():
	var file = File.new()
	file.open(dir_save_path, File.READ)
	latest_path = file.get_var()
	file.close()


func save_dir():
	var file = File.new()
	file.open(dir_save_path, File.WRITE)
	file.store_var(latest_path)
	file.close()

func load_preferences(force_new_file=false):
	var file = File.new()
	# Check for preferences file
	if not file.file_exists(preferences_save_path) or force_new_file: # If no pref file or the force_new_file arg has been enabled, make a new file
		save_preferences()
	file.open(preferences_save_path, File.READ)
	var saved_preferences = file.get_var().split("|")
	top_menu_color = saved_preferences[0]
	text_color = saved_preferences[1]
	background_color = saved_preferences[2]
	file.close()


func save_preferences():
	var file = File.new()
	file.open(preferences_save_path, File.WRITE)
	file.store_var("{top_color}|{top_text_color}|{background_color}".format({
		"top_color": top_menu_color,
		"top_text_color": text_color,
		"background_color": background_color}))
	file.close()


func list_files_in_directory(path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			if file.ends_with(".png"):
				files.append(file)
			elif file.ends_with(".jpg"):
				files.append(file)
	dir.list_dir_end()
	return files


func get_dir(path):
	var dirs = 0
	var dir = ""
	for letter in path:
		if letter == "/":
			dirs += 1
	var current_dirs = 0
	for letter in path:
		if current_dirs == dirs:
			break
		elif letter == "/":
			current_dirs += 1
		dir += letter
	return dir


func item_index(list, item_index):
	var index = 0
	for item in list:
		if item == item_index:
			break
		index += 1
	return index


func img_name(path):
	var path_split = path.split("/")
	return path_split[len(path_split)-1]


func create_timer(wait_time):
	var timer = Timer.new()
	timer.set_wait_time(wait_time)
	timer.set_one_shot(true)
	timer.connect("timeout", timer, "queue_free")
	add_child(timer)
	timer.start()
	return timer


func _on_window_resized():
	$BackgroundColor.set_size(get_viewport().size)
	$"Menu/MenuBar".set_size(Vector2(get_viewport().size.x, 35))
	$"LowerMenu/CollisionBar".set_size(Vector2(get_viewport().size.x, 20))
	$"LowerMenu/CollisionBar".set_position(Vector2(0, get_viewport().size.y-20))
	$"LowerMenu/PopupMenuContainer".set_position(Vector2((get_viewport().size.x/2)-100, get_viewport().size.y+42))
	if not moved:
		$Image.set_position(Vector2(get_viewport().size.x/2, (get_viewport().size.y+35)/2))
	if $Image.get_texture().get_width() > $Image.get_texture().get_height():
		if $Image.get_texture().get_width() > og_size.x:
			if zoom <= 1.0 or $Image.get_texture().get_width() >= get_viewport().size.x:
				var ff = (get_viewport().size.x)/$Image.get_texture().get_width()
				if $Image.get_texture().get_height()*ff <= get_viewport().size.y-35:
					if not zoomed_in:
						if not moved:
							zoom = (get_viewport().size.x)/$Image.get_texture().get_width()
	else:
		if $Image.get_texture().get_height() > og_size.y-35:
			if zoom <= 1.0 or $Image.get_texture().get_height() >= get_viewport().size.y-35:
				var ff = (get_viewport().size.y-35)/$Image.get_texture().get_height()
				if $Image.get_texture().get_width()*ff <= get_viewport().size.x:
					if not zoomed_in:
						if not moved:
							zoom = (get_viewport().size.y-35)/$Image.get_texture().get_height()


func _on_menu_file_pressed(id):
	if id == 0:
		$"Menu/FileDialogOpen".popup()
		$"Menu/FileDialogOpen".set_current_path(latest_path)
	elif id == 2:
		get_tree().quit()


func _on_menu_edit_pressed(id):
	# Rotate Right
	if id == 0:
		_on_menu_view_pressed(0)
		_on_menu_view_pressed(1)
		$Image.set_global_rotation_degrees($Image.get_global_rotation_degrees()+90)
	# Rotate Left
	elif id == 1:
		_on_menu_view_pressed(0)
		_on_menu_view_pressed(1)
		$Image.set_global_rotation_degrees($Image.get_global_rotation_degrees()-90)
	elif id == 2:
		print("Color picker")
	elif id == 4:
		$"Menu/WindowPreferences".popup()


func _on_menu_view_pressed(id):
	if id == 0:
		$Image.set_position(Vector2(get_viewport().size.x/2, (get_viewport().size.y+35)/2))
		$Image.set_offset(Vector2(0, 0))
		moved = false
	elif id == 1:
		zoom = 1.0
		if $Image.get_texture().get_width() > $Image.get_texture().get_height():
			if $Image.get_texture().get_width() > og_size.x:
				if zoom <= 1.0 or $Image.get_texture().get_width() >= get_viewport().size.x:
					var ff = (get_viewport().size.x)/$Image.get_texture().get_width()
					zoom = ff
			var ff = (get_viewport().size.x)/$Image.get_texture().get_width()
			if $Image.get_texture().get_height()*ff > get_viewport().size.y-35:
				ff = (get_viewport().size.y-35)/$Image.get_texture().get_height()
				zoom = ff
		else:
			if $Image.get_texture().get_height() > og_size.y-35:
				if zoom <= 1.0 or $Image.get_texture().get_height() >= get_viewport().size.y-35:
					var ff = (get_viewport().size.y-35)/$Image.get_texture().get_height()
					if $Image.get_texture().get_width()*ff <= get_viewport().size.x:
						zoom = (get_viewport().size.y-35)/$Image.get_texture().get_height()
		zoomed_in = false


func _on_menu_help_pressed(id):
	if id == 0:
		print("About")
	elif id == 1:
		print("Keyboard Shortcuts")

func _on_FileDialogOpen_file_selected(path):
	var image = Image.new()
	var texture = ImageTexture.new()
	image.load(path)
	image.lock()
	texture.create_from_image(image, 1)
	$Image.set_texture(texture)
	latest_path = path
	save_dir()
	var dir = get_dir(path)
	images = list_files_in_directory(dir)
	current_dir = dir
	image_path = path
	current_image = item_index(images, img_name(image_path))
	OS.set_window_title(img_name(image_path) + " - Tezlz Image Viewer")


func _on_Image_texture_changed():
	_on_menu_view_pressed(0)
	if not remember_zoom:
		_on_menu_view_pressed(1)


func _on_Menu_mouse_entered():
	Input.set_custom_mouse_cursor(load("res://cursor_arrow.png"))


func _on_Menu_mouse_exited():
	if current_cursor == 1:
		Input.set_custom_mouse_cursor(load("res://cursor_move.png"))


func _on_CollisionBar_mouse_entered():
	$"LowerMenu/PopupMenuContainer/PopupMenu".bar_entered = true
	$"LowerMenu/PopupMenuContainer/PopupMenu".move_menu()


func _on_CollisionBar_mouse_exited():
	$"LowerMenu/PopupMenuContainer/PopupMenu".move_menu()
	yield(create_timer(0.05), "timeout")
	$"LowerMenu/PopupMenuContainer/PopupMenu".bar_entered = false
	$"LowerMenu/PopupMenuContainer/PopupMenu".move_menu()


func _on_WindowPreferences_popup_hide():
	save_preferences()


func _on_TopBarColor_color_changed(color):
	$"Menu/MenuBar".set_frame_color(color)
	top_menu_color = color.to_html()


func _on_BackgroundColor_color_changed(color):
	$BackgroundColor.set_frame_color(color)
	background_color = color.to_html()


func _on_TopBarTextColor_color_changed(color):
	for button in $"Menu/MenuButtonContainer".get_children():
		button.add_color_override("font_color", color)
		button.add_color_override("font_color_hover", Color(color.r, color.g, color.b, 0.5))
	text_color = color.to_html()


func _on_ResetColorsButton_pressed():
	top_menu_color = og_top_menu_color
	$"Menu/MenuBar".set_frame_color(og_top_menu_color)
	background_color = og_background_color
	$BackgroundColor.set_frame_color(og_background_color)
	text_color = og_text_color
	for button in $"Menu/MenuButtonContainer".get_children():
		button.add_color_override("font_color", og_text_color)
		var og_text_color_argb = Color(og_text_color)
		button.add_color_override("font_color_hover", Color(og_text_color_argb.r, og_text_color_argb.g, og_text_color_argb.b, 0.5))


func _on_PopupMenuContainer_mouse_entered():
	$"LowerMenu/PopupMenuContainer/PopupMenu".menu_entered = true
	$"LowerMenu/PopupMenuContainer/PopupMenu".move_menu()


func _on_PopupMenuContainer_mouse_exited():
	$"LowerMenu/PopupMenuContainer/PopupMenu".move_menu()
	yield(create_timer(0.05), "timeout")
	$"LowerMenu/PopupMenuContainer/PopupMenu".menu_entered = false
	$"LowerMenu/PopupMenuContainer/PopupMenu".move_menu()


func _on_Button_mouse_entered():
	$"LowerMenu/PopupMenuContainer/PopupMenu".button_entered = true
	$"LowerMenu/PopupMenuContainer/PopupMenu".move_menu()


func _on_LeftArrowButton_mouse_exited():
	$"LowerMenu/PopupMenuContainer/PopupMenu".move_menu()
	yield(create_timer(0.05), "timeout")
	$"LowerMenu/PopupMenuContainer/PopupMenu".button_entered = false
	$"LowerMenu/PopupMenuContainer/PopupMenu".move_menu()


func _on_LeftArrowButton_pressed():
	if not current_image == 0:
			current_image -= 1
	else:
		current_image = len(images)-1
	_on_FileDialogOpen_file_selected(current_dir + images[current_image])


func _on_FullscreenButton_pressed():
	OS.window_fullscreen = not OS.window_fullscreen


func _on_RightArrowButton_pressed():
	if not current_image == len(images)-1:
			current_image += 1
	else:
		current_image = 0
	_on_FileDialogOpen_file_selected(current_dir + images[current_image])


func _on_RememberZoomCheck_toggled(button_pressed):
	remember_zoom = button_pressed
