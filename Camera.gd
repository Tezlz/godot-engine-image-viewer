extends Camera2D

var zoomfactor = 1.0

func _ready():
	pass

func _process(delta):
	pass
#	zoom.x = zoomfactor
#	zoom.y = zoomfactor

func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed():
			if event.button_index == BUTTON_WHEEL_UP:
				zoomfactor -= 0.01
			if event.button_index == BUTTON_WHEEL_DOWN:
				zoomfactor += 0.01
